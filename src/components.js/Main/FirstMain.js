import axios from "axios";
import { useState } from "react";
import FirstMainCss from "./FirstMainCss.css";

const FirstMain = () => {
  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: data.email,
      password: data.password,
    };
    axios
      .post("http://localhost:8080/api/users/login", userData)
      .then((response) => {
        window.location.href = "http://localhost:3000/book/all";
        console.log(response);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
    //   .catch(function (error) {
    //     if (error.response.status === 500) {
    //       alert("Server error");
    //     } else {
    //       alert("Successful");
    //     }
    //   });
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const createNewAccount = () => {
    window.location.href = "http://localhost:3000/user/register";
  };

  return (
    <div className="content">
      <div className="flex-div">
        <div className="name-content">
          <h1 className="logo">Facebook</h1>
          <p>Connect with friends and the world around you on Facebook.</p>
        </div>
        <form>
          <input
            type="text"
            placeholder="Email"
            id="email"
            name="email"
            value={data.email}
            onChange={handleChange}
          />
          <input
            type="password"
            placeholder="Password"
            id="password"
            name="password"
            value={data.password}
            onChange={handleChange}
          />
          <button className="login" onClick={handleSubmit}>
            Log In
          </button>
          <a href="/user/forgotPassword">Forgot Password ?</a>
          <hr />
        </form>
        <button className="create-account" onClick={createNewAccount}>Create New Account</button>
      </div>
    </div>
  );
};

export default FirstMain;

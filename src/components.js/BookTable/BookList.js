import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link, useParams } from "react-router-dom";
import BookListCss from "./BookListCss.css";

export default function BookList() {
  const [books, setBooks] = useState([]);
  useEffect(() => {
    loadBooks();
  }, []);

  const loadBooks = async () => {
    const result = await axios.get(`http://localhost:8080/api/book/all`);
    setBooks(result.data);
  };

  const deleteBookById = async (id) => {
    var result = window.confirm("Are you sure to delete?");
    if (result) {
      await axios.delete(
        `http://localhost:8080/api/book/delete/byBookId/${id}`
      );
      loadBooks();
    }
  };

  const createButton = () => {
    window.location.href = "http://localhost:3000/book/create";
  };

  const getEditPage = async(id) => {
          window.location.href = `http://localhost:3000/edit/book/${id}`
    }

  return (
    <div className="bodyList">
      <button className="createButton" onClick={createButton}>
        Add New Book
      </button>
      <table cellPadding="0" cellSpacing="0" border="0">
        <thead>
          <tr>
            <th>Id</th>
            <th>Book Name</th>
            <th>Author</th>
            <th>Book Type</th>
            <th>Book Release Date</th>
          </tr>
        </thead>
        <tbody>
          {books.map((book) => (
            <tr key={book.id}>
              <td>{book.id}</td>
              <td>{book.name}</td>
              <td>{book.author}</td>
              <td>{book.bookType}</td>
              <td>{book.bookReleaseDate}</td>
              <td>
                <button className="editButton" onClick={() => getEditPage(book.id)}>
                  Edit
                </button>
                <button
                  className="deleteButton"
                  onClick={() => deleteBookById(book.id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

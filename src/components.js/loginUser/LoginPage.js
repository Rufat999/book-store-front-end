import axios from "axios";
import { useState } from "react";
import Login from "./Login.css";

const LoginPage = () => {
  const [data, setData] = useState({
    email: "",
    password: "",
  });

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: data.email,
      password: data.password,
    };
    axios
      .post("http://localhost:8080/api/users/login", userData)
      .then((response) => {
        window.location.href = "http://localhost:3000/book/all";
        console.log(response);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
    //   .catch(function (error) {
    //     if (error.response.status === 500) {
    //       alert("Server error");
    //     } else {
    //       alert("Successful");
    //     }
    //   });
  };

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  return (
    <div className="loginBody">
      <form className="loginForm">
        <label htmlFor="email">Email:</label>
        <input
          className="loginEmail"
          type="email"
          id="email"
          name="email"
          value={data.email}
          onChange={handleChange}
        />

        <label htmlFor="password">Password:</label>
        <input
          className="loginPassword"
          type="password"
          id="password"
          name="password"
          value={data.password}
          onChange={handleChange}
        />
        <button type="submit" onClick={handleSubmit}>
          Login
        </button>
      </form>
    </div>
  );
};

export default LoginPage;

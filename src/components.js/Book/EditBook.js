import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";

const EditBook = () => {
  const { id } = useParams();

  const [data, setData] = useState({
    name: "",
    author: "",
    bookType: "",
    bookReleaseDate: 0,
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  useEffect(() => {
    loadBook();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const bookData = {
      name: data.name,
      author: data.author,
      bookType: data.bookType,
      bookReleaseDate: data.bookReleaseDate,
    };
    axios
      .put(`http://localhost:8080/api/book/update/book/${id}`, bookData)
      .then((response) => {
        console.log(response);
        window.location.href = "http://localhost:3000/book/all";
      });
  };

  const onClickBackButton = () => {
    window.location.href = "http://localhost:3000/book/all";
  };

  const loadBook = async (e) => {
    const result = await axios.get(`http://localhost:8080/api/book/byId/${id}`);
    setData(result.data);
  };

  return (
    <div className="container">
      <h3>Edit Book</h3>
      <hr />
      <form>
        <p className="item">
          <label htmlFor="name"> Name </label>
          <input
            type="text"
            name="name"
            id="name"
            value={data.name}
            onChange={handleChange}
          />
        </p>
        <p className="item">
          <label htmlFor="author"> Author </label>
          <input
            type="text"
            name="author"
            id="author"
            value={data.author}
            onChange={handleChange}
          />
        </p>
        <p className="item">
          <label htmlFor="bookType"> Book Type </label>
          <input
            type="text"
            name="bookType"
            id="bookType"
            value={data.bookType}
            onChange={handleChange}
          />
        </p>
        <p className="item">
          <label htmlFor="bookReleaseDate"> Book Release Date </label>
          <input
            type="number"
            name="bookReleaseDate"
            id="bookReleaseDate"
            value={data.bookReleaseDate}
            onChange={handleChange}
          />
        </p>
        <p className="item">
          <input type="submit" onClick={handleSubmit} value="Update" />
        </p>
      </form>

      <input type="submit" onClick={onClickBackButton} value="Back" />
    </div>
  );
};
export default EditBook;

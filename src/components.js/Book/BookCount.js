import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";

export default function BookCount() {
  const [books, setBooks] = useState([]);
  useEffect(() => {
    loadBooks();
  }, []);

  const loadBooks = async () => {
    const result = await axios.get(`http://localhost:8080/api/book/all`);
    setBooks(result.data);
  };

  return (
    <div className="bodyList">
      <h1>{books.length}</h1>
    </div>
  );
}
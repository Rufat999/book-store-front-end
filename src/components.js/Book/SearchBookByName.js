import axios from "axios";
import { useEffect, useState } from "react";
import { Link } from "react-router-dom";

const SearchBookByName = () => {
  const [books, setBooks] = useState([]);
  useEffect(() => {
    loadBooks();
  }, []);

  const loadBooks = async () => {
    const result = await axios.get(`http://localhost:8080/api/book/all`);
    setBooks(result.data);
  };

  const searchHandle = async (event) => {
    let key = event.target.value;
    let result = await axios.get(
      `http://localhost:8080/api/book/search?query=" + ${key}`
    );
    if (result) {
      setBooks(result);
    }
  };

  return (
    <div>
      <div>
        <input placeholder="Search Book By Name" onChange={searchHandle} />
      </div>
      <table cellPadding="0" cellSpacing="0" border="0">
        <thead>
          <tr>
            <th>Id</th>
            <th>Book Name</th>
            <th>Author</th>
          </tr>
        </thead>
        <tbody>
          {books.map((book) => (
            <tr key={book.id}>
              <td>{book.id}</td>
              <td>{book.name}</td>
              <td>{book.author}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
};
export default SearchBookByName;

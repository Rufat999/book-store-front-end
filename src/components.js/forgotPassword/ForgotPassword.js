import axios from "axios";
import { useState, useEffect, Component } from "react";
import ForgotPasswordCss from "../forgotPassword/ForgotPasswordCss.css";

const ForgotPassword = () => {
  const [data, setData] = useState({
    email: "",
    password: "",
    confirmPassword: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      email: data.email,
      password: data.password,
      confirmPassword: data.confirmPassword,
    };
    axios
      .put("http://localhost:8080/api/users/resetPassword", userData)
      .then((response) => {
        // const select = document.getElementById("role");
        // const selectedValue = select.value;
        // if (selectedValue === "user") {
        //   window.location.href = "http://localhost:3000/book/all";
        // }
        // if (selectedValue === "admin") {
        //   window.location.href = "http://localhost:3000/admin/main";
        // }
        window.location.href = "http://localhost:3000/main";
        console.log(response.data.message);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
  };

  return (
    <div>
      <header className="header-section">
        <div className="header-container">
          <div className="w-25">
            <div className="logo-area">
              <a href="http://localhost:3000/main" target="_self">
                <img
                  src="https://i.ibb.co/26J4kF1/facebook-logo.png"
                  alt="Facebook Logo"
                />
              </a>
            </div>
          </div>
        </div>
      </header>
      <div className="content-section">
        <div className="content-container">
          <div className="w-50">
            <div className="help-heading">
              <h2>
                Facebook help you connect and share with <br />
                the people in your life.
              </h2>
            </div>
            <div className="connect-people">
              <img
                src="https://i.ibb.co/9r4FH9W/connect-people.png"
                alt="Connect People"
              />
            </div>
          </div>
          <div className="w-50">
            <div className="main-heading">
              <h1>Forgot Password</h1>
              <h3>It's quick and easy.</h3>
            </div>
            <div className="form-section">
              <form>
                <input
                  type="text"
                  name="email"
                  id="email"
                  placeholder="Email address"
                  value={data.email}
                  onChange={handleChange}
                />
                <input
                  type="password"
                  name="password"
                  id="password"
                  placeholder="Password"
                  value={data.password}
                  onChange={handleChange}
                />
                <input
                  type="password"
                  name="confirmPassword"
                  id="confirmPassword"
                  placeholder="Confirm Password"
                  value={data.confirmPassword}
                  onChange={handleChange}
                />
                <button className="signUp" type="submit" onClick={handleSubmit}>
                  Reset Password
                </button>
              </form>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
};
export default ForgotPassword;

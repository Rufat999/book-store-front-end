const DeleteById = () => {
  const [users, setUsers] = useState([]);
  deleteRow = (id, e) => {
    axios.delete(`http://localhost:8080/api/users/delete/${id}`).then((res) => {
      console.log(res);
      console.log(res.data);

      const users = this.state.users.filter((user) => user.id !== id);
      this.setState({ users });
    });
  };
};
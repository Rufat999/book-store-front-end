import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";

export default function UserCount() {
  const [count, setCount] = useState([]);
  const result = useState();
  useEffect(() => {
    loadCount();
  }, []);

  const loadCount = async () => {
    const result = await axios.get(`http://localhost:8080/api/users/count`);
    setCount(result.data);
    console.log(result.data);
  };
  return <div className="bodyList">{count}</div>;
}
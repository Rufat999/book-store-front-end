import axios from "axios";
import React from "react";
import { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { Component } from "react";

const EditUser = () => {
  const { id } = useParams();

  const [data, setData] = useState({
    surname: "",
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  useEffect(() => {
    loadUser();
  }, []);

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      surname: data.surname,
      name: data.name,
      email: data.email,
      password: data.password,
    };
    axios
      .put(`http://localhost:8080/api/users/edit/${id}`, userData)
      .then((response) => {
        console.log(response);
        window.location.href = "http://localhost:3000/all";
      });
  };

  const loadUser = async (e) => {
    const result = await axios.get(`http://localhost:8080/api/users/${id}`);
    setData(result.data);
  };
  return (
    <div className="bodyRegister">
      <h1>Sign Up</h1>
      <legend>
        <span className="number">1</span>Your basic info
      </legend>
      <form>
        <label htmlFor="surname">Surname:</label>
        <input
          type="text"
          id="surname"
          name="surname"
          value={data.surname}
          onChange={handleChange}
        />

        <label htmlFor="name">Name:</label>
        <input
          type="text"
          id="name"
          name="name"
          value={data.name}
          onChange={handleChange}
        />

        <label htmlFor="email">Email:</label>
        <input
          type="email"
          id="email"
          name="email"
          value={data.email}
          onChange={handleChange}
        />

        <label htmlFor="password">Password:</label>
        <input
          type="password"
          id="password"
          name="email"
          value={data.password}
          onChange={handleChange}
        />
        <button onClick={handleSubmit}>Update</button>
      </form>
    </div>
  );
};

export default EditUser;

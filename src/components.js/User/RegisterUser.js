import axios from "axios";
import { useState, useEffect, Component } from "react";
import { Link, useHistory, useParams } from "react-router-dom";

const RegisterUser = () => {
  const [data, setData] = useState({
    surname: "",
    name: "",
    email: "",
    password: "",
  });

  const handleChange = (e) => {
    const value = e.target.value;
    setData({
      ...data,
      [e.target.name]: value,
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    const userData = {
      surname: data.surname,
      name: data.name,
      email: data.email,
      password: data.password,
    };
    axios
      .post("http://localhost:8080/api/users/register", userData)
      .then((response) => {
        const select = document.getElementById("role");
        const selectedValue = select.value;
        if (selectedValue === "user") {
          window.location.href = "http://localhost:3000/book/all";
        }
        if (selectedValue === "admin") {
          window.location.href = "http://localhost:3000/admin/main";
        }
        // window.location.href = "http://localhost:3000/all";
        console.log(response.data.message);
      })
      .catch((error) => {
        if (error.response.status === 400) {
          alert(error.response.data);
        }
      });
    //==========================================================
    // .catch(function (error) {
    //   if (error.response.data.status === 400) {
    //     alert(error.response.data);
    //   } else {
    //     alert("Enter something!");
    //   }
    // });
  };

  return (
    <div className="bodyRegister">
      <form>
        <h1>Sign Up</h1>

        <fieldset>
          <legend>
            <span className="number">1</span>Your basic info
          </legend>
          <label htmlFor="surname">Surname:</label>
          <input
            type="text"
            id="surname"
            name="surname"
            value={data.surname}
            onChange={handleChange}
          />

          <label htmlFor="name">Name:</label>
          <input
            type="text"
            id="name"
            name="name"
            value={data.name}
            onChange={handleChange}
          />

          <label htmlFor="email">Email:</label>
          <input
            type="email"
            id="email"
            name="email"
            value={data.email}
            onChange={handleChange}
          />

          <label htmlFor="password">Password:</label>
          <input
            type="password"
            id="password"
            name="password"
            value={data.password}
            onChange={handleChange}
          />
        </fieldset>
        <fieldset>
          <label>Job Role:</label>
          <select id="role" name="role">
            <option value="user">User</option>
            <option value="admin">Admin</option>
          </select>
        </fieldset>
        <button type="submit" onClick={handleSubmit}>
          Sign Up
        </button>
      </form>
    </div>
  );
};
export default RegisterUser;

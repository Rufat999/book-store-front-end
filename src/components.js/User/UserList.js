import React from "react";
import axios from "axios";
import { useState, useEffect } from "react";
import { Link } from "react-router-dom";

export default function UserList() {
  const [users, setUsers] = useState([]);
  useEffect(() => {
    loadUsers();
  }, []);

  const loadUsers = async () => {
    const result = await axios.get(`http://localhost:8080/api/users/all`);
    setUsers(result.data);
  };

  const deleteUserById = async (id) => {
    var result = window.confirm("Are you sure to delete?");
    if(result){
    await axios.delete(`http://localhost:8080/api/users/delete/${id}`);
    loadUsers();
    }
  };

  return (
    <div className="bodyList">
      <table cellPadding="0" cellSpacing="0" border="0">
        <thead>
          <tr>
            <th>Id</th>
            <th>Surname</th>
            <th>Name</th>
            <th>Email</th>
            <th>Password</th>
          </tr>
        </thead>
        <tbody>
          {users.map((user) => (
            <tr key={user.id}>
              <td>{user.id}</td>
              <td>{user.surname}</td>
              <td>{user.name}</td>
              <td>{user.email}</td>
              <td>{user.password}</td>
              <td>
                <button className="edit">
                  <Link to={`edit/${user.id}`}>Edit</Link>
                </button>
                <button
                  className="delete"
                  onClick={() => deleteUserById(user.id)}
                >
                  Delete
                </button>
              </td>
            </tr>
          ))}
        </tbody>
      </table>
    </div>
  );
}

const AdminInterface = () => {

  const onClickAllUsersButton = () => {
    window.location.href="http://localhost:3000/user/all";
  }

  const onClickAllBooksButton = () => {
    window.location.href="http://localhost:3000/book/all"
  }

  return (
    <div>
      <button onClick={onClickAllUsersButton}>All Users</button>
      <button onClick={onClickAllBooksButton}>All Books</button>
    </div>
  );
};

export default AdminInterface

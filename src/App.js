import React from "react";
import { BrowserRouter, Switch, Route } from "react-router-dom";
import UserList from "./components.js/User/UserList";
import EditUser from "./components.js/User/EditUser";
import LoginPage from "./components.js/loginUser/LoginPage";
import AddBook from "./components.js/Book/AddBook";
import BookList from "./components.js/BookTable/BookList";
import EditBook from "./components.js/Book/EditBook";
import BookCount from "./components.js/Book/BookCount";
import UserCount from "./components.js/User/UserCount";
import AdminInterface from "./components.js/User/AdminInterface";
import FirstMain from "./components.js/Main/FirstMain";
import SearchBookBookByName from "./components.js/Book/SearchBookByName";
import SignUp from "./components.js/signUp/SignUp";
import ForgotPassword from "./components.js/forgotPassword/ForgotPassword";

function App() {
  return (
    <div>
      <BrowserRouter>
        <Switch>
          <Route path="/user/register">
            <SignUp />
          </Route>
          <Route path="/user/all">
            <UserList />
          </Route>
          <Route path="/user/forgotPassword">
            <ForgotPassword />
          </Route>
          <Route exact path="/edit/:id">
            <EditUser />
          </Route>
          {/* <Route path="/user/login">
            <LoginPage />
          </Route> */}
          <Route path="/book/create">
            <AddBook />
          </Route>
          <Route path="/book/all">
            <BookList />
          </Route>
          <Route path="/edit/book/:id">
            <EditBook />
          </Route>
          <Route path="/book/count">
            <BookCount />
          </Route>
          <Route path="/user/count">
            <UserCount />
          </Route>
          <Route path="/admin/main">
            <AdminInterface />
          </Route>
          <Route path="/main">
            <FirstMain />
          </Route>
          <Route path="/book/search">
            <SearchBookBookByName />
          </Route>
        </Switch>
      </BrowserRouter>
    </div>
  );
}

export default App;
